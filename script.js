const buttonSend = document.getElementById('button-send');
const posts = document.getElementById('posts');
const message = document.getElementById('message');

buttonSend.addEventListener('click', () =>{
    createPost(message.value);
})

const createButton = (text, color) => {
    const button = document.createElement('button');
    const classe = text === 'Editar' ? 'edit' : 'delet';
    button.classList.add(classe);
    button.innerText = text;
    button.style.backgroundColor = color;
    return button;
}

const createDiv = (classe) => {
    const div = document.createElement('div');
    div.classList.add(classe);
    return div;
}

function createPost(text){
    const div = createDiv('post');
    const p = document.createElement('p')

    const divButtons = createDiv('div-buttons');
    const buttonEdit = createButton('Editar', '#F7A75D');
    const buttonDelet = createButton('Excluir', '#F75D5D');
    const buttonChange = createButton('Enviar', 'green');
    const inputChanger = document.createElement('input');

    buttonChange.addEventListener('click', updateMessage);
    divButtons.appendChild(inputChanger);
    divButtons.appendChild(buttonChange);

    div.appendChild(divButtons);
    p.innerText = text;
    buttonDelet.addEventListener('click', () => {
        div.remove();
    })

    divButtons.appendChild(buttonEdit)
    divButtons.appendChild(buttonDelet)
    p.classList.add('text');
    div.appendChild(divButtons)
    div.appendChild(p)
    posts.appendChild(div);
    message.value = '';
}
